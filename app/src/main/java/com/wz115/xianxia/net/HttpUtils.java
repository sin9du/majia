package com.wz115.xianxia.net;

import android.content.Intent;

import com.google.gson.Gson;
import com.wz115.xianxia.Base.MyApp;
import com.wz115.xianxia.activity.NativieActivity;
import com.wz115.xianxia.activity.SplashActivity;
import com.wz115.xianxia.bean.loginbean;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by 愤怒的艾泽拉斯 on 2018/5/27.
 */

public class HttpUtils {

    private final OkHttpClient mOkHttpClient;
    private final Request.Builder mBuilder;
    private final Gson mGson;
    private SplashActivity mContext;


    public HttpUtils(){
        mOkHttpClient = new OkHttpClient();
        mBuilder = new Request.Builder();
        mGson = new Gson();
    }
    public void WebState(SplashActivity context, String url){
        mContext=context;
        final Request request = mBuilder.url(url).build();
        Call lCall = mOkHttpClient.newCall(request);

        lCall.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mContext.onFailLoad();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String lResponseData = response.body().string();
                loginbean lLoginbean = mGson.fromJson(lResponseData, loginbean.class);
                if (lLoginbean.isSuccess()){
                    mContext.onloadSuccess(lLoginbean.getAppConfig());
                }else{
                    Intent intent = new Intent(MyApp.getsContext(), NativieActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MyApp.getsContext().startActivity(intent);
                    mContext.finish();
                }
            }
        });
    }

}
