package com.wz115.xianxia.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.wz115.xianxia.Base.BaseFragment;
import com.wz115.xianxia.R;
import com.wz115.xianxia.utils.pdshow;
import com.wz115.xianxia.utils.px2dp;

/**
 * Created by 愤怒的艾泽拉斯 on 2018/5/27.
 */

public class Tab3 extends BaseFragment {
    private WebView mWebView;
    //    private String  url="http://www.cashptc.cn/ziyuan/List.html";
    private String url = "http://vzhuan.cn/category/wangzhuanjiqiao";
    //    private String  url="http://m.7m.com.cn/blive/matchs.html";
    private pdshow mPdshow;
    private LinearLayout mLlnodata;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPdshow = new pdshow(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View lView = inflater.inflate(R.layout.fragment_tab3, container, false);
        mWebView = lView.findViewById(R.id.Tab_webview3);
        mLlnodata = lView.findViewById(R.id.ll_nodata);

        websets(mWebView, url, mPdshow, mLlnodata);
        setMarginTop();
        return lView;
    }

    private void setMarginTop() {
//        int margintop=-110;
        int margintop = -150;
        int lI = px2dp.dip2px(getContext(), margintop);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(mWebView.getLayoutParams());
        lp.setMargins(0, lI, 0, 0);
        mWebView.setLayoutParams(lp);
    }
}
