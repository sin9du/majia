package com.wz115.xianxia.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.wz115.xianxia.Base.BaseFragment;
import com.wz115.xianxia.R;
import com.wz115.xianxia.utils.pdshow;
import com.wz115.xianxia.utils.px2dp;

/**
 * Created by 愤怒的艾泽拉斯 on 2018/5/27.
 */

public class Tab1 extends BaseFragment {

    private static final String TAG = "Tab1";
    private WebView mWebView;
    private LinearLayout mLlnodata;
    private String url = "http://www.cashptc.cn/";
//    private String  url="http://vzhuan.cn/category/wangzhuanzixun";
//    private String  url="https://www.cash28.com.cn/";
//    private String  url="http://m.7m.com.cn/live/matchs.html";

    private pdshow mPdshow;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPdshow = new pdshow(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab1, container, false);
        mWebView = view.findViewById(R.id.Tab_webview1);
        setMarginTop();
        mLlnodata = view.findViewById(R.id.ll_nodata);
        websets(mWebView, url, mPdshow, mLlnodata);
        return view;
    }

    private void setMarginTop() {
        int margintop = -200;
        int lI = px2dp.dip2px(getContext(), margintop);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(mWebView.getLayoutParams());
        lp.setMargins(0, lI, 0, 0);
        mWebView.setLayoutParams(lp);
    }

}
