package com.wz115.xianxia.utils;

import android.app.ProgressDialog;
import android.support.v4.app.FragmentActivity;

/**
 * Created by 愤怒的艾泽拉斯 on 2018/5/27.
 */

public class pdshow {


    private final FragmentActivity context;
    private ProgressDialog mPd;

    public pdshow(FragmentActivity activity) {
        context=activity;
    }

    public void showpd(){
        if (mPd==null){
            mPd = new ProgressDialog(context);
            mPd.setMessage("正在加载中，请稍后");
            mPd.show();
        }else {
            mPd.show();
        }
    }

    public void closepd(){
        if (mPd!=null){
            mPd.dismiss();
        }
    }
}
