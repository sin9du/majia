package com.wz115.xianxia.utils;

import android.view.View;
import android.view.ViewGroup;

/**
 * Created by 愤怒的艾泽拉斯 on 2018/5/27.
 */

public class SetMargin {

    public static void setMargins (View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }
}
