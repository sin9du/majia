package com.wz115.xianxia.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by 愤怒的艾泽拉斯 on 2018/5/27.
 */

public class FragmentAdapter extends FragmentPagerAdapter {
    //继承FragmentPagerAdapter类 ,并自定义的构造器
    private List<Fragment> fragments;
    public FragmentAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments =fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
