package com.wz115.xianxia.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.wz115.xianxia.R;
import com.wz115.xianxia.bean.loginbean;
import com.wz115.xianxia.net.HttpUtils;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by 愤怒的艾泽拉斯 on 2018/5/27.
 */

public class SplashActivity extends Activity {
    private String url="http://cdjbjg.cn/biz/getAppConfig?appid=helian825";
    private HttpUtils mHttpUtils;
    private SplashActivity mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_splash);
        mContext=SplashActivity.this;
        TimerTask ts = new TimerTask() {
            @Override
            public void run() {
                load();
            }
        };
        new Timer().schedule(ts, 2000);
    }

    private void load() {
        if (mHttpUtils==null){
            mHttpUtils = new HttpUtils();
            mHttpUtils.WebState(mContext,url);
        }else{
            mHttpUtils.WebState(mContext,url);
        }
    }

    public void onFailLoad() {
        showNavipage();
    }

    public void onloadSuccess(loginbean.AppConfigBean lLogininfo) {
        if (lLogininfo.getShowWeb().equals("1")){
            showHomePage(lLogininfo.getUrl());
        }else if (lLogininfo.getIsUpdate().equals("1")){
            showUpDatapage(lLogininfo.getUpdateUrl());
        }else {
            showNavipage();
        }
    }

    private void showNavipage() {
        Intent lIntent = new Intent(SplashActivity.this,NativieActivity.class);
        startActivity(lIntent);
        finish();
    }

    private void showHomePage(String url) {
        Intent lIntent = new Intent(SplashActivity.this,MainActivity.class);
        lIntent.putExtra("url",url);
        startActivity(lIntent);
        finish();
    }

    private void showUpDatapage(String update_url) {
        Intent it = new Intent();
        it.setClass(SplashActivity.this, WebUpDataActivity.class);
        it.putExtra("updata",update_url);
        startActivity(it);
        finish();
    }
}
