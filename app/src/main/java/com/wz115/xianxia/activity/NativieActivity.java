package com.wz115.xianxia.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wz115.xianxia.R;
import com.wz115.xianxia.adapter.FragmentAdapter;
import com.wz115.xianxia.fragment.Tab1;
import com.wz115.xianxia.fragment.Tab2;
import com.wz115.xianxia.fragment.Tab3;
import com.wz115.xianxia.fragment.Tab4;

import java.util.ArrayList;

/**
 * Created by 愤怒的艾泽拉斯 on 2018/5/27.
 */

public class NativieActivity extends FragmentActivity implements View.OnClickListener {

    private ViewPager mViewPager;
    private ArrayList<Fragment> mFragments;
    private LinearLayout mTabMain;
    private LinearLayout mTabCommunity;
    private LinearLayout mTabShopping;
    private LinearLayout mTabMe;
    private ImageView mImageTabMain;
    private ImageView mImageTabCommunity;
    private ImageView mImageTabShopping;
    private ImageView mImageTabMe;
    private FragmentAdapter mAdapter;
    private TextView mTitle;
    private View mImageView;
    private TextView tab1_bottom;
    private TextView tab2_bottom;
    private TextView tab3_bottom;
    private TextView tab4_bottom;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nativiteactivity);
        mImageView = findViewById(R.id.back);
        init();
        initClickListener();
    }

    public void showback(final WebView webView){
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.goBack();
            }
        });
    }

    private void init() {
        mViewPager = findViewById(R.id.view_pager);
        mTabMain = (LinearLayout) findViewById(R.id.id_tab_main);
        mTabCommunity = (LinearLayout) findViewById(R.id.id_tab_community);
        mTabShopping = (LinearLayout) findViewById(R.id.id_tab_shopping);
        mTabMe = (LinearLayout) findViewById(R.id.id_tab_me);
        mImageTabMain = (ImageView) findViewById(R.id.tab_main_icon_grey);
        mImageTabCommunity = (ImageView) findViewById(R.id.tab_community_icon_grey);
        mImageTabShopping = (ImageView) findViewById(R.id.tab_shopping_cart_icon_grey);
        mImageTabMe = (ImageView) findViewById(R.id.tab_me_icon_grey);
        tab1_bottom = findViewById(R.id.tab1_title);
        tab2_bottom = findViewById(R.id.tab2_title);
        tab3_bottom = findViewById(R.id.tab3_title);
        tab4_bottom = findViewById(R.id.tab4_title);
        mTitle = findViewById(R.id.nativity_title);
        mFragments = new ArrayList<Fragment>();

        Fragment mTab1 = new Tab1();
        Fragment mTab2 = new Tab2();
        Fragment mTab3 = new Tab3();
        Fragment mTab4 = new Tab4();

        mFragments.add(mTab1);
        mFragments.add(mTab2);
        mFragments.add(mTab3);
        mFragments.add(mTab4);
        mAdapter = new FragmentAdapter(getSupportFragmentManager(), mFragments);
        mViewPager.setAdapter(mAdapter);
        setSelect(0);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            //滑动时 改变图标状态
            @Override
            public void onPageSelected(int position) {
                int currentItem = mViewPager.getCurrentItem();
                initTabImage();
                switch (currentItem) {
                    case 0:
                        mImageTabMain.setImageResource(R.drawable.tab1s);
                        mTitle.setText("首页");
//                        tab1_bottom.setTextColor(getColor(R.color.textselect));
                        break;
                    case 1:
                        mImageTabCommunity.setImageResource(R.drawable.tab2s);
                        mTitle.setText("学习");
//                        tab2_bottom.setTextColor(getColor(R.color.textselect));
                        break;
                    case 2:
                        mImageTabShopping.setImageResource(R.drawable.tab3s);
                        mTitle.setText("任务");
//                        tab3_bottom.setTextColor(getColor(R.color.textselect));
                        break;
                    case 3:
                        mImageTabMe.setImageResource(R.drawable.tab4s);
                        mTitle.setText("方法");
//                        tab4_bottom.setTextColor(getColor(R.color.textselect));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
    //初始的图标状态(滑动和点击事件改变的时候都要初始化)
    private void initTabImage() {
        mImageTabMain.setImageResource(R.drawable.tab1n);
        mImageTabCommunity.setImageResource(R.drawable.tab2n);
        mImageTabShopping.setImageResource(R.drawable.tab3n);
        mImageTabMe.setImageResource(R.drawable.tab4n);
//        tab1_bottom.setTextColor(getColor(R.color.textnormal));
//        tab2_bottom.setTextColor(getColor(R.color.textnormal));
//        tab3_bottom.setTextColor(getColor(R.color.textnormal));
//        tab4_bottom.setTextColor(getColor(R.color.textnormal));
    }
    //设置图标点击监听器
    private void initClickListener() {
        mTabMain.setOnClickListener(this);
        mTabCommunity.setOnClickListener(this);
        mTabShopping.setOnClickListener(this);
        mTabMe.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_tab_main:
                setSelect(0);
                mTitle.setText("首页");
                break;
            case R.id.id_tab_community:
                setSelect(1);
                mTitle.setText("学习");
                break;
            case R.id.id_tab_shopping:
                setSelect(2);
                mTitle.setText("任务");
                break;
            case R.id.id_tab_me:
                mTitle.setText("方法");
                setSelect(3);
                break;
        }
    }

    private void setSelect(int i) {
        initTabImage();
        switch (i) {
            case 0:
                mImageTabMain.setImageResource(R.drawable.tab1s);
//                tab1_bottom.setTextColor(getColor(R.color.textselect));
                break;
            case 1:
                mImageTabCommunity.setImageResource(R.drawable.tab2s);
                break;
            case 2:
                mImageTabShopping.setImageResource(R.drawable.tab3s);
                break;
            case 3:
                mImageTabMe.setImageResource(R.drawable.tab4s);
                break;
            default:
                break;
        }
        mViewPager.setCurrentItem(i);
    }

}
