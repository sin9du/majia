package com.wz115.xianxia.Base;

import android.app.Application;
import android.content.Context;

import java.util.HashSet;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by 愤怒的艾泽拉斯 on 2018/5/27.
 */

public class MyApp extends Application {
    public static Context sContext;

    public static Context getsContext() {
        return sContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        //初始化sdk
        JPushInterface.setDebugMode(true);//正式版的时候设置false，关闭调试
        JPushInterface.init(this);
        //建议添加tag标签，发送消息的之后就可以指定tag标签来发送了
        Set<String> set = new HashSet<>();
       set.add("andfixdemo");//名字任意，可多添加几个
       JPushInterface.setTags(this, set, null);//设置标签
    }
}
